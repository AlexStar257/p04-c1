const container = document.getElementById("container");
const cardTemplate = document.getElementById("cardTemplate");
const fragment = document.createDocumentFragment();

const peticionAjax = () => {
  const http = new XMLHttpRequest();
  const url = "https://jsonplaceholder.typicode.com/photos";

  http.onreadystatechange = function () {
    //validar la respuesta de la peticion
    if (this.status == 200 && this.readyState == 4) {
      const json = JSON.parse(this.responseText);

      for (const datos of json) {
        const clone = cardTemplate.content.cloneNode(true);
        clone.querySelector("#openModal").id = "openModal" + datos.id;
        clone.querySelector(".modal__link").href = "#openModal" + datos.id;
        clone.querySelector(".modal__img").src = datos.url;
        clone.querySelector("#img").src = datos.thumbnailUrl;
        clone.querySelector("#title").textContent = datos.title;
        clone.querySelector(".id").textContent = datos.id;

        clone.querySelector(".album").textContent = datos.albumId;
        fragment.appendChild(clone);
      } // termina el for

      container.innerHTML = "";
      container.appendChild(fragment);
    } //termina el if
  }; //termina la funcion
  http.open("GET", url, true);
  http.send();
}; //termina la funcion del ajax

document.getElementById("btnPeticion").addEventListener("click", peticionAjax);